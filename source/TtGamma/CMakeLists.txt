
# Declare the name of this package:
atlas_subdir( TtGamma None )


# This package uses ROOT:
find_package( ROOT REQUIRED COMPONENTS Core Gpad Tree Hist RIO MathCore Graf )

# Custom definitions needed for this package:
add_definitions( -g )

# Generate a CINT dictionary source file:
atlas_add_root_dictionary( TtGamma _cintDictSource
                           ROOT_HEADERS Root/LinkDef.h
                           EXTERNAL_PACKAGES ROOT )

# Build a library that other components can link against:
atlas_add_library( TtGamma Root/*.cxx Root/*.h Root/*.icc
                   TtGamma/*.h TtGamma/*.icc TtGamma/*/*.h
                   TtGamma/*/*.icc ${_cintDictSource} 
                   PUBLIC_HEADERS TtGamma
                   LINK_LIBRARIES TopAnalysis
				  TopCPTools
                                  TopEventSelectionTools
                                  TopConfiguration
                                  TopCorrections
                                  TopEvent
                                  TopParticleLevel
                                  TopPartons
                                  TopObjectSelectionTools
                                  TopSystematicObjectMaker
                          #        TopFakes
				  TopDataPreparation
				  #TopHLUpgrade
				  AsgTools
                                  GammaORToolsLib
                                  IsolationToolLib
				  xAODEgamma
				  AsgTools
                                  AthContainers                                  
				  PATCoreLib			  
                                  PATInterfaces
                                  TrigBunchCrossingTool
                                  TrigConfInterfaces
                                  TrigConfxAODLib
                                  TrigDecisionToolLib
                                  TriggerMatchingToolLib
                                  TrigTauMatchingLib
                                  GoodRunsListsLib
                                  EgammaAnalysisInterfacesLib
				  ElectronPhotonFourMomentumCorrectionLib
                                  ElectronPhotonSelectorToolsLib
                                  ElectronEfficiencyCorrectionLib
                                  ElectronPhotonShowerShapeFudgeToolLib
                                  PhotonEfficiencyCorrectionLib
                                  MuonMomentumCorrectionsLib
                                  MuonSelectorToolsLib
                                  MuonEfficiencyCorrectionsLib
                                  TauAnalysisToolsLib
                                  CalibrationDataInterfaceLib
                                  xAODBTaggingEfficiencyLib
                                  JetCalibToolsLib
                                  JetCPInterfaces
                                  JetUncertaintiesLib
                                  JetInterface
                                  JetMomentToolsLib
                                  JetSelectorToolsLib
                                  METInterface
                                  METUtilitiesLib
                                  IsolationSelectionLib
                                  IsolationCorrectionsLib
                                  PathResolver
                                  TopConfiguration
                                  TopEvent
                                  PileupReweightingLib
                                  AssociationUtilsLib
                                  JetJvtEfficiencyLib
                                  PMGToolsLib
                                  InDetTrackSystematicsToolsLib
                                  BoostedJetTaggersLib
				  FTagAnalysisInterfacesLib
				  MuonAnalysisInterfacesLib
				  #TriggerAnalysisInterfacesLib
				  TrigGlobalEfficiencyCorrectionLib
				  PMGAnalysisInterfacesLib
				  JetAnalysisInterfacesLib
                                  ${ROOT_LIBRARIES}
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} )

# Install data files from the package:
atlas_install_data( share/* )

# Install user scripts
atlas_install_scripts( scripts/*.py )

# Build the executables of the package:
#atlas_add_executable( myexec_TtGamma
#                      util/myexec_TtGamma.cxx
#                      LINK_LIBRARIES TopCPTools
#                                     TopEventSelectionTools
#                                     TopConfiguration
#                                     TopCorrections
#                                     TopEvent
#                                     TopParticleLevel
#                                     TopPartons
#                                     TopObjectSelectionTools
#                                     TopSystematicObjectMaker
#                                     FakeBkgTools
#				     TopDataPreparation
#                                     TopHLUpgrade
#                                     
#                                     TopAnalysis )

