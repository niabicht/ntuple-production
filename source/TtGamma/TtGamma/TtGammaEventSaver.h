#ifndef TTGAMMA_TTGAMMAEVENTSAVER_H
#define TTGAMMA_TTGAMMAEVENTSAVER_H

#include "TopAnalysis/EventSaverFlatNtuple.h"
#include "TopConfiguration/ConfigurationSettings.h"

#include <fstream>
#include <AsgTools/AnaToolHandle.h>  
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODEgamma/Photon.h"
#include "GammaORTools/IVGammaORTool.h"  
#include <utility>
#include <set>

#include "PhotonEfficiencyCorrection/AsgPhotonEfficiencyCorrectionTool.h"
#include "IsolationCorrections/IIsolationCorrectionTool.h"
#include "IsolationCorrections/IsolationCorrection.h"


namespace top
{

    class TopConfig;

    class TtGammaEventSaver : public top::EventSaverFlatNtuple {

    public:

        TtGammaEventSaver();
        virtual ~TtGammaEventSaver(){}
          
        using top::EventSaverFlatNtuple::initialize;
        void initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches);
        void saveEvent(const top::Event& event);
        bool isHadronFakePhoton(const xAOD::Photon* phPtr);

    private:

        AsgPhotonIsEMSelector* m_photonTightIsEMSelector;
        AsgPhotonIsEMSelector* m_photonLooseIsEMSelector;
        AsgPhotonEfficiencyCorrectionTool* m_photonEfficiencyCorrectionTool;
	asg::AnaToolHandle<IVGammaORTool> m_vgamma_tool;

	//CP::IIsolationCorrectionTool *m_isoCorrTool;
	
        std::shared_ptr<top::TopConfig> m_configMine;


        //--------- additional custom variables for the output

        std::vector<float> m_ph_topoetcone20;
        std::vector<float> m_ph_topoetcone30;
        std::vector<float> m_ph_topoetcone40;
	
        std::vector<float> m_ph_ptcone20;
        std::vector<float> m_ph_ptcone30;
        std::vector<float> m_ph_ptcone40;
	//---------- precision recommendations
	/*std::vector<float> m_ph_topoetcone20_IsoCorrTool; 
	std::vector<float> m_ph_topoetcone40_IsoCorrTool;
	std::vector<float> m_ph_ptcone20_TightTTVA_pt500;	
	std::vector<float> m_ph_ptcone20_TightTTVA_pt1000;
	std::vector<float> m_ph_ptvarcone30_TightTTVA_pt500;
	std::vector<float> m_ph_ptvarcone30_TightTTVA_pt1000;*/
	
        std::vector<char> m_ph_isoFCTCO;
        std::vector<char> m_ph_isoFCT;
        std::vector<char> m_ph_isoFCL;
        std::vector<char> m_ph_isTight;
        std::vector<char> m_ph_isTight_daod;
        std::vector<unsigned int> m_ph_isEM_Tight;
        std::vector<char> m_ph_isLoose;
        std::vector<int> m_ph_truthType;
        std::vector<int> m_ph_truthOrigin;
        std::vector<int> m_ph_mc_pid;
        std::vector<float> m_ph_mc_pt;
        std::vector<float> m_ph_mc_eta;
        std::vector<float> m_ph_mc_phi;
        std::vector<float> m_ph_mcel_pt;
        std::vector<float> m_ph_mcel_eta;
        std::vector<float> m_ph_mcel_phi;
        std::vector<float> m_ph_mcel_dr;
        std::vector<uint32_t> m_ph_OQ;
        std::vector<uint32_t> m_ph_author;
        std::vector<int> m_ph_conversionType;
        std::vector<float> m_ph_caloEta;

        std::vector<float> m_ph_SF_ID;
        std::vector<float> m_ph_SF_ID_unc;
        std::vector<float> m_ph_SF_iso;
        std::vector<float> m_ph_SF_iso_unc;

        std::vector<char> m_el_isoGradient;
        std::vector<int> m_el_mc_pid;
        std::vector<float> m_el_mc_charge;
        std::vector<float> m_el_mc_pt;
        std::vector<float> m_el_mc_eta;
        std::vector<float> m_el_mc_phi;


        // photon shower shape variables

        std::vector<float> m_ph_rhad1;
        std::vector<float> m_ph_rhad;        
        std::vector<float> m_ph_reta;
        std::vector<float> m_ph_weta2;
        std::vector<float> m_ph_rphi;        
        std::vector<float> m_ph_ws3;    //a.k.a. weta1
        std::vector<float> m_ph_wstot;  //a.k.a wtot
        std::vector<float> m_ph_fside;  //a.k.a. fracm
        std::vector<float> m_ph_deltaE;
        std::vector<float> m_ph_eratio; //a.k.a. DEmaxs1
        std::vector<float> m_ph_emaxs1;
        std::vector<float> m_ph_f1;
        std::vector<float> m_ph_e277;

        // flag for hadron fake        
        std::vector<char> m_ph_isHadronFake;    

        std::unordered_map<std::string, float> m_triggerPrescales;

        ClassDef(top::TtGammaEventSaver, 0);
	std::vector<int> m_ph_mc_parent_pid;
	std::vector<int> m_ph_mc_production;
	char m_event_in_overlap;
    };
}

#endif