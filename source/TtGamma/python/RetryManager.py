from pandatools.queryPandaMonUtils import query_tasks
from pandatools.PBookCore import PBookCore
import os
'''
Check API definitions here
PBookCore: /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/PandaClient/1.4.18/lib/python2.6/site-packages/pandatools
or 
cd $ATLAS_LOCAL_PANDACLIENT_PATH/lib/python2.6/site-packages/
'''

#ts, url, tasklist = query_tasks(jeditaskid=None, username=None, limit=10000, taskname=None, status=None, superstatus=None,
 #               reqid=None, days=None, metadata=False, sync=False, verbose=False)
ts, url, tasklist = query_tasks(taskname="user.hpotti*.04_Apr_20*", status="failed,finished", sync=True, days=40)
ts, url, tasklist2 = query_tasks(taskname="group.phys-top*.15_Apr_20*", status="running", sync=True, days=30)
pbook = PBookCore()

print(len(tasklist))
counter = 0

doneList = []
for task in tasklist2:
    outName = str(task["datasets"][-1]["containername"])
    if "group.phys-top" in outName:
        doneList.append(outName)
    if os.path.exists("/data_ceph/harish/extra_v3/"+outName):
        print(outName)

for task in tasklist:
    #pbook.killAndRetry(task["jeditaskid"], newOpts={'excludedSite': ''})
    #pbook.retry(task["jeditaskid"], newOpts={'excludedSite': ''})
    outName = str(task["datasets"][-1]["containername"])
    if "user.hpotti" not in outName:
        #print(outName)
        continue
    newName = outName.replace("user.hpotti", "group.phys-top").replace("v1.04", "_tqgamma.15")
    
    tag = ""
    if ("r9364" in outName):
        tag = "mc16a/"
    elif ("r10201" in outName):                    
        tag = "mc16d/"
    elif ("r10724" in outName):
        tag = "mc16e/"
    elif("periodAll" in outName):
        tag = "allData/"
    else:
        print("Error: Strange dataset campaign ", outName)
        sys.exit(1)

    if (os.path.exists("/data_ceph/harish/extra_v3/"+newName) or os.path.exists("/data_ceph/harish/v3_tqGamma/"+tag+newName) ):
        counter += 1
        pass
    else:
        #print(newName)
        pass


    if newName in doneList:
        #os.system("mv /data_ceph/harish/extra_v3/"+newName+"  /data_ceph/harish/v3_tqGamma/"+tag)
        #os.system("mv /data_ceph/harish/v3_tqGamma/"+tag+outName+" /data_ceph/harish/extra_v3/")
        print(newName)
        pass


print(counter)
