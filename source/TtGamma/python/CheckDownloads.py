path="/data_ceph/harish/v3_tqGamma/"
path ="/data_ceph/harish/extra_v3/"

import rucio.client
import os, sys

rcl = rucio.client.Client()

scope = "user.hpotti"
scope = "group.phys-top"

dids = list( rcl.list_dids(scope, {"name":"user.hpotti*v1.04_Apr_20*root"}) )
dids = list( rcl.list_dids(scope, {"name":"group.phys-top*15_Apr_20*root"}) )

print("Checking %s datasets" %len(dids))
#print(dids)
for did in dids:
    opath = ""
    #print(opath+did+"/")
    flist = list(rcl.list_files(scope, did))
    incomplete = False
    if ("r9364" in did):
        opath = path+"mc16a/"
    elif ("r10201" in did):
        opath = path+"mc16d/"
    elif ("r10724" in did):
        opath = path+"mc16e/"
    elif("periodAll" in did):
        opath = path+"allData/"
        did = ""
    else:
        print("Error: Strange dataset campaign ", did)
        sys.exit(1)
    opath = path
    for f in flist:
        if ( os.path.exists(opath+did+"/"+f["name"]) ):
            pass
        else:
            incomplete = True
            #print(did+"/"+f["name"]+" has not been downloaded")
            break
        
    if(incomplete):
        print(did)
        os.system("cd %s; rucio get %s" %(opath,did))
        pass

